from scipy.spatial.distance import euclidean

from distance_selector.address_to_coords import get_coordinates_from_address
from distance_selector.coordinate_conversion import spherical_to_cartesian

EARTH_MEAN_RADIUS = EMR = 6371.0087714  # km


def resolve_coordinates_for_addresses(addresses, apikey):
    return list(map(get_coordinates_from_address, addresses, [apikey for item in addresses]))


def euclidean_from_coordinates(point1: dict, point2: dict) -> float:
    cartesian_point1 = spherical_to_cartesian(EMR, point1['lat'], point1['lng'])
    cartesian_point2 = spherical_to_cartesian(EMR, point2['lat'], point2['lng'])

    return round(euclidean(cartesian_point1, cartesian_point2), 9)


def distance_between_addresses(addresses: list, apikey: str = None) -> dict:
    address_coordinates = resolve_coordinates_for_addresses(addresses, apikey)

    distances = {}
    for num, coords1 in enumerate(address_coordinates):
        for num2, coords2 in enumerate(address_coordinates[num+1:]):
            distance = euclidean_from_coordinates(coords1, coords2)
            distances[f'{num} <-> {num+num2+1}'] = distance

    return distances
