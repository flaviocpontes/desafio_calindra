from urllib.parse import urlencode

import pytest

from applications.api import create_api
from config import TestConfig


@pytest.fixture
def test_client():
    api = create_api(TestConfig)
    return api.test_client()


def test_invalid_addresses(test_client):
    addresses = [
        'R. Primeiro de Março, S/n - Praça XV, Rio de Janeiro - RJ, '
        '20010-090',
        'R. dos Bobos, 0'
    ]
    query_string = urlencode([('address', address) for address in addresses])

    resp = test_client.get(f'/address_distance?{query_string}')

    assert resp.json == {'erro': 'Address not found',
                         'address': 'R. dos Bobos, 0'}
    assert resp.status_code == 400


def test_simple_2_addresses(test_client):
    addresses = [
        'R. Primeiro de Março, S/n - Praça XV, Rio de Janeiro - RJ, 20010-090',
        'R. Pinheiro Machado, s/nº - Laranjeiras, Rio de Janeiro - RJ, 22231-901'
    ]
    query_string = urlencode([('address', address) for address in addresses])

    resp = test_client.get(f'/address_distance?{query_string}')

    assert resp.json == {
        'addresses': {'0': 'R. Primeiro de Março, S/n - Praça XV, Rio de Janeiro - '
                           'RJ, 20010-090',
                      '1': 'R. Pinheiro Machado, s/nº - Laranjeiras, Rio de Janeiro - '
                           'RJ, 22231-901'},
        'closest': {'0': 'R. Primeiro de Março, S/n - Praça XV, Rio de Janeiro - RJ, '
                         '20010-090',
                    '1': 'R. Pinheiro Machado, s/nº - Laranjeiras, Rio de Janeiro - '
                         'RJ, 22231-901',
                    'distance': 3.867596839},
        'distances': {'0 <-> 1': 3.867596839},
        'farthest': {'0': 'R. Primeiro de Março, S/n - Praça XV, Rio de Janeiro - RJ, '
                          '20010-090',
                     '1': 'R. Pinheiro Machado, s/nº - Laranjeiras, Rio de Janeiro - '
                          'RJ, 22231-901',
                     'distance': 3.867596839}}


def test_simple_3_addresses(test_client):
    addresses = [
        'R. Primeiro de Março, S/n - Praça XV, Rio de Janeiro - RJ, 20010-090',
        'R. Pinheiro Machado, s/nº - Laranjeiras, Rio de Janeiro - RJ, 22231-901',
        '7, R. do Catete, 359 - Catete, Rio de Janeiro - RJ, 22220-001'
    ]
    query_string = urlencode([('address', address) for address in addresses])

    resp = test_client.get(f'/address_distance?{query_string}')
    assert resp.json == {
        'addresses': {'0': 'R. Primeiro de Março, S/n - Praça XV, Rio de Janeiro - '
                           'RJ, 20010-090',
                      '1': 'R. Pinheiro Machado, s/nº - Laranjeiras, Rio de Janeiro - '
                           'RJ, 22231-901',
                      '2': '7, R. do Catete, 359 - Catete, Rio de Janeiro - RJ, '
                           '22220-001'},
        'closest': {'1': 'R. Pinheiro Machado, s/nº - Laranjeiras, Rio de Janeiro - '
                         'RJ, 22231-901',
                    '2': '7, R. do Catete, 359 - Catete, Rio de Janeiro - RJ, '
                         '22220-001',
                    'distance': 0.880414167},
        'distances': {'0 <-> 1': 3.867596839,
                      '0 <-> 2': 3.228680598,
                      '1 <-> 2': 0.880414167},
        'farthest': {'0': 'R. Primeiro de Março, S/n - Praça XV, Rio de Janeiro - RJ, '
                          '20010-090',
                     '1': 'R. Pinheiro Machado, s/nº - Laranjeiras, Rio de Janeiro - '
                          'RJ, 22231-901',
                     'distance': 3.867596839}}


def test_8_global_addresses(test_client):
    addresses = [
        'Catedral Metropolitana de São Sebastião do Rio de Janeiro',
        'Mount Rushmore National Memorial',
        'Cidade Proibida, Jingshan Front Street, Dongcheng, Beijing, China',
        'Monte Fuji, Kitayama, Fujinomiya, Shizuoka, Japan',
        'Museu do Louvre, Rue de Rivoli, Paris, France',
        'Pirâmide de Quéops, Al Haram, Nazlet El-Semman, Al Giza Desert, Egypt',
        'Machu Picchu',
        'Ilha de Páscoa'
    ]
    query_string = urlencode([('address', address) for address in addresses])

    resp = test_client.get(f'/address_distance?{query_string}')
    assert resp.json == {
        'addresses': {'0': 'Catedral Metropolitana de São Sebastião do Rio de Janeiro',
                      '1': 'Mount Rushmore National Memorial',
                      '2': 'Cidade Proibida, Jingshan Front Street, Dongcheng, '
                           'Beijing, China',
                      '3': 'Monte Fuji, Kitayama, Fujinomiya, Shizuoka, Japan',
                      '4': 'Museu do Louvre, Rue de Rivoli, Paris, France',
                      '5': 'Pirâmide de Quéops, Al Haram, Nazlet El-Semman, Al Giza '
                           'Desert, Egypt',
                      '6': 'Machu Picchu',
                      '7': 'Ilha de Páscoa'},
        'closest': {'2': 'Cidade Proibida, Jingshan Front Street, Dongcheng, Beijing, '
                         'China',
                    '3': 'Monte Fuji, Kitayama, Fujinomiya, Shizuoka, Japan',
                    'distance': 2016.554076568},
        'distances': {'0 <-> 1': 8738.561115192,
                      '0 <-> 2': 12458.024232156,
                      '0 <-> 3': 12665.553125988,
                      '0 <-> 4': 8397.053168334,
                      '0 <-> 5': 8914.046837542,
                      '0 <-> 6': 3244.480996548,
                      '0 <-> 7': 6315.404215107,
                      '1 <-> 2': 8917.857094492,
                      '1 <-> 3': 8419.124312106,
                      '1 <-> 4': 7020.155400502,
                      '1 <-> 5': 9415.28937482,
                      '1 <-> 6': 6716.430426046,
                      '1 <-> 7': 7417.150260365,
                      '2 <-> 3': 2016.554076568,
                      '2 <-> 4': 7657.981943359,
                      '2 <-> 5': 7119.722585468,
                      '2 <-> 6': 12366.602518132,
                      '2 <-> 7': 11983.250179487,
                      '3 <-> 4': 8793.983579175,
                      '3 <-> 5': 8662.412520946,
                      '3 <-> 6': 12123.386437528,
                      '3 <-> 7': 11162.082464925,
                      '4 <-> 5': 3176.344902528,
                      '4 <-> 6': 9029.915709356,
                      '4 <-> 7': 11252.102070181,
                      '5 <-> 6': 10325.17577392,
                      '5 <-> 7': 12163.551204354,
                      '6 <-> 7': 4051.937244597},
        'farthest': {'0': 'Catedral Metropolitana de São Sebastião do Rio de Janeiro',
                     '3': 'Monte Fuji, Kitayama, Fujinomiya, Shizuoka, Japan',
                     'distance': 12665.553125988}}
