# Desafio Calindra Backend

[![gitlab-ci](https://gitlab.com/flaviocpontes/desafio_calindra/badges/master/pipeline.svg)](https://gitlab.com/flaviocpontes/desafio_calindra)
[![codecov](https://codecov.io/gl/flaviocpontes/desafio_calindra/branch/master/graph/badge.svg)](https://codecov.io/gl/flaviocpontes/desafio_calindra)

### ATENÇÃO

__Para usar localmente ou implantar a API é rpeciso ter uma  chave da API do Google Maps conforme explicado em [Get Started with Google Maps Platform](https://developers.google.com/maps/gmp-get-started) e ter o seu valor na variável de ambiente `GOOGLE_MAPS_API_KEY`.__  

## Instalação

Para instalar é preciso ter o python 3.8.4 ou superior instalado e rodar o comando `pip install -r requirements.txt`

Para iniciar use o comando: `gunicorn --bind 0.0.0.0:8080 --workers 1 --threads 8 wsgi:api`. A API se encotrará em `http://localhost:8080/address_distance`

#### Docker

É possível instalar usando o docker. Para isso é necessário ter uma chave da API do Google Maps conforme o guia [Get Started with Google Maps Platform](https://developers.google.com/maps/gmp-get-started). Esta chave será usada como argumento da construção da imagem.

O comando para o build é: `docker build --build-arg apikey=<CHAVE_DA_API> .`

## Testes

Para rodar os testes é preciso instalar as dependências de desenvolvimento através do comando `pip install -r requirements_dev.txt` 

Posteriormente é preciso atribuir a variável de ambiente `GOOGLE_MAPS_API_KEY` para a chave de API recuperada acima e rodar o comando `pytest --cov=applications --cov=distance_selector tests/`

## Uso

*Todas as distâncias estão em Km*

A API se encontra em `https://desafio-calindra-wbroee2yoa-uc.a.run.app/address_distance` e seu uso se faz por qualquer cliente http como o `curl` por exemplo.

O comando com o curl par ase usar é o seguinte: `curl https://desafio-calindra-wbroee2yoa-uc.a.run.app/address_distance --get --data-urlencode address=Cristo\ Redentor --data-urlencode address=Monte\ Fuji --data-urlencode address=Forbidden\ City`, que deverá gerar a seguinte saída:

```json
{"addresses":{"0":"Cristo Redentor","1":"Monte Fuji","2":"Forbidden City"},"closest":{"0":"Cristo Redentor","2":"Forbidden City","distance":7726.002799436},"distances":{"0 <-> 1":12666.008150838,"0 <-> 2":7726.002799436,"1 <-> 2":9287.844083506},"farthest":{"0":"Cristo Redentor","1":"Monte Fuji","distance":12666.008150838}}
``` 
Já com o excelente HTTPie (instalável pelo `pip install httpie`) o comando fica: `http GET https://desafio-calindra-wbroee2yoa-uc.a.run.app/address_distance address=="Cristo Redentor" address=="Monte Fuji" address=="Forbidden City"` e deve gerar o a seguinte saída:

```
HTTP/1.1 200 OK
Alt-Svc: h3-29=":443"; ma=2592000,h3-27=":443"; ma=2592000,h3-T050=":443"; ma=2592000,h3-Q050=":443"; ma=2592000,h3-Q046=":443"; ma=2592000,h3-Q043=":443"; ma=2592000,quic=":443"; ma=2592000; v="46,43"
Content-Length: 326
Date: Wed, 29 Jul 2020 23:32:47 GMT
Server: Google Frontend
content-type: application/json

{
    "addresses": {
        "0": "Cristo Redentor",
        "1": "Monte Fuji",
        "2": "Forbidden City"
    },
    "closest": {
        "0": "Cristo Redentor",
        "2": "Forbidden City",
        "distance": 7726.002799436
    },
    "distances": {
        "0 <-> 1": 12666.008150838,
        "0 <-> 2": 7726.002799436,
        "1 <-> 2": 9287.844083506
    },
    "farthest": {
        "0": "Cristo Redentor",
        "1": "Monte Fuji",
        "distance": 12666.008150838
    }
}
 
```