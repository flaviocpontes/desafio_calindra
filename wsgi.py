from applications.api import create_api
from config import BaseConfig

api = create_api(BaseConfig)

if __name__ == '__main__':
    api.run(host='0.0.0.0', port=8000)