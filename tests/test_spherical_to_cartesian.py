from distance_selector.coordinate_conversion import spherical_to_cartesian


def test_radius_0():
    assert spherical_to_cartesian(0, 0, 0) == (0, 0, 0)


def test_latitude_90():
    assert spherical_to_cartesian(1, 90, 0) == (0, 0, 1)


def test_latitude_60():
    assert spherical_to_cartesian(1, 60, 0) == (0.5, 0, 0.866025404)


def test_latitude_45():
    assert spherical_to_cartesian(1, 45, 0) == (0.707106781, 0, 0.707106781)


def test_latitude_30():
    assert spherical_to_cartesian(1, 30, 0) == (0.866025404, 0, 0.5)


def test_minus_30():
    assert spherical_to_cartesian(1, -30, 0) == (0.866025404, 0, -0.5)


def test_minus_45():
    assert spherical_to_cartesian(1, -45, 0) == (0.707106781, 0, -0.707106781)


def test_minus_60():
    assert spherical_to_cartesian(1, -60, 0) == (0.5, 0, -0.866025404)


def test_minus_90():
    assert spherical_to_cartesian(1, -90, 0) == (0, 0, -1)


def test_0_0_maximum_x():
    assert spherical_to_cartesian(1, 0, 0) == (1, 0, 0)


def test_longitude_minus_180():
    assert spherical_to_cartesian(1, 0, -180) == (-1.0, -0.0, 0.0)


def test_longitude_minus_150():
    assert spherical_to_cartesian(1, 0, -150) == (-0.866025404, -0.5, 0.0)


def test_longitude_minus_135():
    assert spherical_to_cartesian(1, 0, -135) == (-0.707106781, -0.707106781, 0.0)


def test_longitude_minus_120():
    assert spherical_to_cartesian(1, 0, -120) == (-0.5, -0.866025404, 0.0)


def test_longitude_minus_90():
    assert spherical_to_cartesian(1, 0, -90) == (0, -1, 0)


def test_longitude_minus_60():
    assert spherical_to_cartesian(1, 0, -60) == (0.5, -0.866025404, 0.0)


def test_longitude_minus_45():
    assert spherical_to_cartesian(1, 0, -45) == (0.707106781, -0.707106781, 0.0)


def test_longitude_minus_30():
    assert spherical_to_cartesian(1, 0, -30) == (0.866025404, -0.5, 0.0)


def test_longitude_30():
    assert spherical_to_cartesian(1, 0, 30) == (0.866025404, 0.5, 0.0)


def test_longitude_45():
    assert spherical_to_cartesian(1, 0, 45) == (0.707106781, 0.707106781, 0.0)


def test_longitude_60():
    assert spherical_to_cartesian(1, 0, 60) == (0.5, 0.866025404, 0.0)


def test_longitude_90():
    assert spherical_to_cartesian(1, 0, 90) == (0.0, 1.0, 0.0)


def test_longitude_120():
    assert spherical_to_cartesian(1, 0, 120) == (-0.5, 0.866025404, 0.0)


def test_longitude_135():
    assert spherical_to_cartesian(1, 0, 135) == (-0.707106781, 0.707106781, 0.0)


def test_longitude_150():
    assert spherical_to_cartesian(1, 0, 150) == (-0.866025404, 0.5, 0.0)


def test_longitude_180():
    assert spherical_to_cartesian(1, 0, 180) == (-1.0, 0.0, 0.0)
