import googlemaps


class NoLocationFound(Exception):
    def __init__(self, msg):
        self.msg = msg


def get_coordinates_from_address(address: str, api_key: str) -> dict:
    gmaps = googlemaps.Client(key=api_key)
    geocode_results = gmaps.geocode(address)

    if not len(geocode_results):
        raise NoLocationFound(address)

    return geocode_results[0].get('geometry').get('location')
