from config import TestConfig
from distance_selector.services import euclidean_from_coordinates as calculate_distance, \
    distance_between_addresses


def test_calculate_distance_same_point():
    point = {'lat': 22.045, 'lng': 54}
    distance = calculate_distance(point, point)

    assert 0 == distance


def test_calculate_pole_distance():
    point1 = {'lat': -90, 'lng': 0}
    point2 = {'lat': 90, 'lng': 0}
    earth_diameter = 12742.0175428

    distance = calculate_distance(point1, point2)

    assert earth_diameter == distance


def test_opposite_longitudes_90_minus90():
    point1 = {'lat': 0, 'lng': 90}
    point2 = {'lat': 0, 'lng': -90}
    earth_diameter = 12742.0175428

    distance = calculate_distance(point1, point2)

    assert earth_diameter == distance


def test_opposite_longitudes_0_180():
    point1 = {'lat': 0, 'lng': 0}
    point2 = {'lat': 0, 'lng': 180}
    earth_diameter = 12742.0175428

    distance = calculate_distance(point1, point2)

    assert earth_diameter == distance


def test_opposite_diagonal_lat45_long45():
    point1 = {'lat': 45, 'lng': 45}
    point2 = {'lat': -45, 'lng': -135}
    earth_diameter = 12742.0175428

    distance = calculate_distance(point1, point2)

    assert earth_diameter == distance


def test_distance_between_2_adresses():
    addresses = [
        '7, R. do Catete, 359 - Catete, Rio de Janeiro - RJ, 22220-001',
        'R. Pinheiro Machado, s/nº - Laranjeiras, Rio de Janeiro - RJ, 22231-901'
    ]

    result = distance_between_addresses(addresses, apikey=TestConfig().GOOGLE_MAPS_API_KEY)

    print(result)