import os

import pytest

from distance_selector.address_to_coords import get_coordinates_from_address, NoLocationFound

API_KEY = os.getenv('GOOGLE_MAPS_API_KEY')


def test_address_found():
    address = 'Universidade Federal Fluminense - Campus Gragoatá'

    coordinates = get_coordinates_from_address(address, api_key=API_KEY)

    assert coordinates == {'lat': -22.8986567, 'lng': -43.131651}


def test_invalid_address():
    address = 'Rua dos Bobos, número 0'

    with pytest.raises(NoLocationFound):
        coordinates = get_coordinates_from_address(address, api_key=API_KEY)
