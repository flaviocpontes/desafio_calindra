import numpy as np


def spherical_to_cartesian(radius, latitude, longitude):
    radian_lat = np.math.radians(latitude)
    radian_long = np.math.radians(longitude)
    x = radius * np.cos(radian_lat) * np.cos(radian_long)
    y = radius * np.cos(radian_lat) * np.sin(radian_long)
    z = radius * np.sin(radian_lat)
    return round(x, 9), round(y, 9), round(z, 9)
