import http

from flask import Flask, request, jsonify, url_for

from distance_selector.address_to_coords import NoLocationFound
from distance_selector.services import distance_between_addresses


def create_api(config_obj):
    api = Flask(__name__)
    api.config.from_object(config_obj())

    @api.errorhandler(NoLocationFound)
    def address_not_found(error):
        resp = jsonify({'erro': 'Address not found',
                        'address': error.msg})
        resp.status_code = http.HTTPStatus.BAD_REQUEST
        return resp

    @api.route('/', methods=['GET'])
    def welcome():
        return jsonify({
            'message': 'Welcome to the address distance calculator',
            'uri': url_for('.compare_addresses', _scheme='https', _external=True)
        })

    @api.route('/address_distance', methods=['GET'])
    def compare_addresses():
        addresses = request.args.getlist('address')
        distances = distance_between_addresses(addresses, apikey=api.config.get('GOOGLE_MAPS_API_KEY'))
        min_distance = min(distances.items(), key=lambda x: x[1])
        closest = min_distance[0].split(' <-> ')[0], min_distance[0].split(' <-> ')[1], min_distance[1]
        max_distance = max(distances.items(), key=lambda x: x[1])
        farthest = max_distance[0].split(' <-> ')[0], max_distance[0].split(' <-> ')[1], max_distance[1]

        return jsonify({
            'addresses': {num: address for (num, address) in enumerate(addresses)},
            'distances': distances,
            'closest': {
                closest[0]: addresses[int(closest[0])],
                closest[1]: addresses[int(closest[1])],
                'distance': closest[2]
            },
            'farthest': {
                farthest[0]: addresses[int(farthest[0])],
                farthest[1]: addresses[int(farthest[1])],
                'distance': farthest[2]
            }
        })

    return api
