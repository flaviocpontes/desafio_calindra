FROM python:3.8.4-slim AS BUILDER

ARG apikey
ENV GOOGLE_MAPS_API_KEY $apikey

ADD . /app
WORKDIR /app
RUN pip install -r requirements_dev.txt
RUN pip install -r requirements.txt
RUN pytest --cov=applications --cov=distance_selector tests/

FROM python:3.8.4-slim AS DEPLOY
ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt
CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 wsgi:api