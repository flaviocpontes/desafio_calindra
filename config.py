import os


class BaseConfig:
    GOOGLE_MAPS_API_KEY = os.getenv('GOOGLE_MAPS_API_KEY')


class TestConfig(BaseConfig):
    TESTING = True
